/**
 * Created by Chintan on 28-11-2015.
 */
'use strict';

var app = angular.module('app',['ngRoute']);

app.config(function ($routeProvider) {

    $routeProvider.when('/', {

        templateUrl: '../views/search.user.html',
        controller:'userController'
    })

        .when('/add', {
        templateUrl: '../views/addorupdate.user.html',
        controller: 'userController'
    })
        .when('/edit', {
            templateUrl: '../views/addorupdate.user.html',
            controller: 'userController'
        })
    .otherwise({redirectTo:'/'});

});


app.run(function () {

    window.location='#/';
});