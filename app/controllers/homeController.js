/**
 * Created by Chintan on 28-11-2015.
 */

(function () {
    'use strict';

    angular.module('app').controller('homeController',['$scope','httpService','$rootScope','logger',homeController]);

    function homeController($scope,$httpService,$rootScope,$logger)
    {

        init();
        function init() {

            $scope.userList = [{
                "id" : 20001,
                "fname": "Jane",
                "lname": "Doe",
                "email": "jane.doe@gmail.com",
                "lob_id" : "1",
                "lob_description" : "Sales and Marketing",
                "addresses": [
                    {
                        "line1": "line1",
                        "line2": "line2",
                        "line3": "line3",
                        "line4": "line4",
                        "city": "city",
                        "state": "state",
                        "zip": "zip"
                    },
                    {
                        "line1": "line1",
                        "line2": "line2",
                        "line3": "line3",
                        "line4": "line4",
                        "city": "city",
                        "state": "state",
                        "zip": "zip"
                    }
                ],
                "phones": [
                    {
                        "number": "5188589666",
                        "type": "work"
                    },
                    {
                        "number": "5188589645",
                        "type": "home"
                    },
                    {
                        "number": "5188589623",
                        "type": "cell"
                    }
                ]
            }];
            getUsers();

        }



        $scope.deleteUser = function (userId) {
            $httpService.post('http://localhost:8080/userregistration/deleteUser?userId='+userId)
                .then(function (data) {
                    $logger.logSuccess('Deleted data successfully', data, true);
                },error);
        }


        $scope.editUser = function (user) {
            $rootScope.user = user;
            window.location = '#/edit';

        }

        function getUsers() {
            $httpService.get('http://192.168.1.10:8080/userregistration/getUsers')
                .then(function (users) {
                    $scope.userList = users;
                },error);
        }

        function error(reason) {
            var msg = 'Error saving data. ' + reason;
            $logger.logError(msg, reason, true);
        }


    }

})();


