/**
 * Created by Chintan on 06-12-2015.
 */

(function () {
    'use strict';

    angular.module('app').controller('userController',['$scope','$rootScope','logger','httpService',userController]);

    function userController($scope,$rootScope,$logger,$httpService) {


        init();

        function init() {
            $scope.user ={};

           if(angular.isDefined($rootScope.user)){
               $scope.user = $rootScope.user;
           }
        }



        $scope.cancel = function () {
            $scope.user = {};
            window.location = '#/';
        }

        $scope.saveUserInfo = function (user) {
            if(angular.isDefined(user)){
                $httpService.post('http://localhost:8080/userregistration/save',user)
                    .then(success,error());
            }
            $scope.cancel();
        }

        function success(data) {
            $logger.logSuccess('Saved data successfully', saveResult, true);
        }

       function error(reason) {
           var msg = 'Error saving data. ' + reason;
           $logger.logError(msg, reason, true);
        }


        $scope.addAddress=function(address) {
            if (angular.isDefined(address)) {
                $scope.user.addresses.push(address);
                $scope.user.address={};
            }
        };

        $scope.editAddress = function (address) {
            if(angular.isDefined(address)){
                $scope.user.address = address;
                $scope.removeAddress(address);
            }
        }

        $scope.removeAddress = function (address){
            if(angular.isDefined(address)) {
                var index = $scope.user.addresses.indexOf(address);
                $scope.user.addresses.splice(index, 1);
            }
        };

        $scope.addPhone = function(phone){
            if(angular.isDefined(phone)){
                $scope.user.phones.push(phone);
                $scope.user.phone = {};
            }
        };

        $scope.editPhone = function (phone) {
            if(angular.isDefined(phone)){
                $scope.user.phone = phone;
                $scope.removePhone(phone);
            }
        }

        $scope.removePhone= function(phone){
            if(angular.isDefined(phone)) {
                var index = $scope.user.phones.indexOf(phone);
                $scope.user.phones.splice(index,1);
            }
        };
    }
})();