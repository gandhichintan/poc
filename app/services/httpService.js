/**
 * Created by Chintan on 11-12-2015.
 */

(function () {
    'use strict';

    var serviceId = 'httpService';

    // TODO: replace app with your module name
    angular.module('app').factory(serviceId, ['$http','$q', httpService]);

    function httpService($http, $q) {
        // Define the functions and properties to reveal.
        var service = {
            post: post,
            get:get
        };

        return service;


        function get(url,data) {
            var deferred = $q.defer();
            $http.get(url,data)
                .success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        }

        function post(url, model) {
            var deferred = $q.defer();
            $http.post(url, model)
                .success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        }



        //function remove(url, model) {
        //    var deferred = $q.defer();
        //    $http.delete(url, model)
        //		.success(deferred.resolve).error(deferred.reject);
        //    return deferred.promise;
        //}




        //#region Internal Methods

        //#endregion
    }
})();
