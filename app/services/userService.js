/**
 * Created by Chintan on 28-11-2015.
 */

(function () {
    'use strict';

    angular.module('app').factory('userService',['$http',userService]);

    function userService($http) {
        var userService = {};

        userService.getAllUsers = function () {
            return $http.get('/getAllUsers');
        };


        userService.addUser = function (user) {
            return $http.post('addUser',user);
        }

        userService.updateUser = function (user) {
            return $http.post('/updateUser',user);
        }

        return userService;
    }

})();
