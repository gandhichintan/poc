/**
 * Created by Chintan on 11-12-2015.
 */

(function () {
    'use strict';

    var app = angular.module('app');

    // Configure Toastr
    toastr.options.timeOut = 4000;
    toastr.options.positionClass = 'toast-bottom-right';



    var config = {
        appErrorPrefix: '[POC Error] ', //Configure the exceptionHandler decorator
        docTitle: 'POC: ',
        showToast: true,
        showSuccessToast: true,
        showErrorToast: true
    };

    app.value('config', config);

    app.config(['$logProvider', function ($logProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
    }]);

})();
